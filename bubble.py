import random
from datetime import datetime

def bubblesort(list):
    is_sorted = False
    while not is_sorted:
        i = 0
        is_sorted = True
        for element in list:
            if i < len(list) - 1 and element > list[i+1]:
                is_sorted = False
                temp = list[i+1]
                list[i+1] = list[i]
                list[i] = temp
            i += 1
        #print(list)
    return list
before_tmie = datetime.now()
print("Before: "+str(before_tmie))
my_list = []
for i in range(0, 1000):
    x = random.randint(1, 100000)
    my_list.append(x)

my_sorted_list = bubblesort(my_list)
after_time = datetime.now()
process_time = after_time-before_tmie
print(f"Sorted list: {my_sorted_list}")
print("After: "+str(after_time))
print("Process time: "+str(process_time))
